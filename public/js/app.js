Crafty.init(500, 350, document.getElementById('game'));

var socket = io.connect('http://localhost');
var square = Crafty.e('2D, Canvas, Color, Fourway')
    .attr({x: pos.x, y: pos.y, w: 50, h: 50})
    .color('blue')
    .fourway(100)
    .bind('Move', _.debounce(function() {
      if (pos.x == this.x && pos.y == this.y) {
	return;
      }

      pos.x = this.x;
      pos.y = this.y;
      socket.emit('pos', pos);
    }));

socket.on('move', function(data) {
  pos.x = data.x;
  pos.y = data.y;
  square.attr(pos);
});

socket.on('message', function(data) {
  console.log(data);
});
