var http = require('http'),
    path = require('path'),
    express = require('express'),
    app = express();

var server = http.Server(app),
    io = require('socket.io')(server),
    pos = {x: 30, y: 30},
    id = 0;

app.set('view engine', 'jade');
app.set('views', 'views');
app.use(express.static(path.join(__dirname, 'public')));

app.get('/', function(req, res) {
  res.render('index', {
    title: 'pretty simple game',
    pos: pos,
    id: id
  });
  id++;
});

io.on('connection', function(socket) {
  console.log('> connection');
  socket.on('pos', function(data) {
    console.log(socket.id);
    pos.x = data.x;
    pos.y = data.y;
    socket.broadcast.emit('move', pos);
  });
});

server.listen(80);
